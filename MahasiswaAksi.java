package com.praktikum6;
import java.util.Scanner;
import com.praktikum6.Mahasiswa;

public class MahasiswaAksi {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan tahun daftar = ");
        int tahun = input.nextInt();
        Mahasiswa mahasiswa1 = new Mahasiswa("A11", "Master", 4.0, 24, "20020222");
        mahasiswa1.getProgdi("A11");
        mahasiswa1.getTagihanSks();
        mahasiswa1.ipkStatus();
        mahasiswa1.getTahun(tahun);
        mahasiswa1.getMhsSemester(tahun);
        // mahasiswa1.getUmur(2002);
    }
}
